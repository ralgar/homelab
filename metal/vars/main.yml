---
common:
  # Name (path) of the venv, using the root user's home as the base.
  # Ex. A value of 'kolla-venv' will become '/root/kolla-venv'
  kolla_venv: /root/kolla-venv

  # Path to your SSH pubkey file. This will be used to access the node.
  ssh_pubkey_file: ~/.ssh/id_ed25519.pub

storage:
  # Target block device for the Openstack host's root filesystem.
  root_device: /dev/disk/by-id/nvme-WD_BLACK_SN770_1TB_23020Q804222

  cinder:
    # Target block device(s) for Cinder "standard" (HDD) tier.
    devices:
      - /dev/disk/by-id/wwn-0x6b8ca3a0faf798002bd0bf3c11919471

  swift:
    # Target block device(s) for Swift.
    devices:
      - /dev/disk/by-id/wwn-0x6b8ca3a0faf798002d7240bf3ecf84b7
      - /dev/disk/by-id/wwn-0x6b8ca3a0faf798002d7240f041b207bc
      - /dev/disk/by-id/wwn-0x6b8ca3a0faf798002d72410a433d4109
      - /dev/disk/by-id/wwn-0x6b8ca3a0faf798002d72413745f2e841

network:
  # A domain to use for the internal OpenStack infrastructure.
  domain: homelab.internal

  # A hostname/subdomain to assign to the AIO OpenStack node.
  hostname: openstack

  # A list of two public DNS resolvers to use for the network.
  dns_servers: ['1.1.1.1', '1.0.0.1']

  # Configure the OpenStack internal provider network - where SSH and the
  #  OpenStack WebUI and APIs will be exposed.
  # This should connect to your internal (private) LAN.
  internal:
    interface: eno1
    ip_address: 10.254.20.11
    network_cidr: 10.254.20.0/24
    gateway_addr: 10.254.20.1

  # Configure external provider network(s), where OpenStack can create
  #  publicly-accessible IP addresses for your infrastructure.
  # These should be internal DMZs, or public subnets assigned by your ISP.
  # NOTE: If using VLANs, allocate a range (eg. 10-14 or 20-29).
  external:
    interface: eno2
    type: vlan                          # Must be one of 'flat' or 'vlan'.
    networks:
      - name: dmz0                      # Name to assign the OpenStack network.
        vlan_id: 10                     # Specify a VLAN ID. Unused when network type is 'flat'.
        network_cidr: 10.254.10.0/30    # Network address in CIDR notation.
        gateway_addr: 10.254.10.1       # IP address of the default gateway.
        dhcp_enabled: false             # Toggle the OpenStack DHCP service on this subnet. (default: true)
        dns_servers: ['1.1.1.1']        # Manually configure DNS server(s) for the subnet.

      - name: dmz1
        vlan_id: 11
        network_cidr: 10.254.11.0/30
        gateway_addr: 10.254.11.1
        dhcp_enabled: false
        dns_servers: ['1.1.1.1']

      - name: dmz2
        vlan_id: 12
        network_cidr: 10.254.12.0/24
        gateway_addr: 10.254.12.1
        dhcp_pool_start: 10.254.12.10   # Start IP of the DHCP pool (managed by OpenStack).
        dhcp_pool_end: 10.254.12.254    # End IP of the DHCP pool (managed by OpenStack).

      - name: dmz3
        vlan_id: 13
        network_cidr: 10.254.13.0/24
        gateway_addr: 10.254.13.1
        dhcp_pool_start: 10.254.13.10   # Start IP of the DHCP pool (managed by OpenStack).
        dhcp_pool_end: 10.254.13.254    # End IP of the DHCP pool (managed by OpenStack).

services:
  # Designate is the OpenStack DNS service.
  designate:

    # Configure Designate as a recursive resolver. This can be a useful hack
    #  if your router has very substandard capabilities. Otherwise, there are
    #  probably better ways to configure DNS resolution for your network.
    recursive_lookup: false
